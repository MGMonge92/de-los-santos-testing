<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'permalink',
        'provider_id',
        'thumbnail',
        'seller',
    ];
}
