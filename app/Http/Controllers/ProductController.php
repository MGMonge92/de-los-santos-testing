<?php

namespace App\Http\Controllers;

use App\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ProductController
{
    public function index(Request $request)
    {
        $offset = $request->input('offset', 0);

        $client = new Client(['base_uri' => 'https://api.mercadolibre.com']);

        try {
            $response = $client->get("/sites/MLA/search?offset={$offset}&limit=50&category=MLA5224&access_token=APP_USR-3737365735154941-060817-4b5973ec3fcf1f851ad520ce374e528e-31503793");
        } catch (Exception $ex) {
            dd($ex->getMessage());
        }

        $products = json_decode($response->getBody()->getContents());

        return view('products')->with(['products' => $products, 'offset' => $offset]);
    }

    public function store(Request $request)
    {
        $client = new Client(['base_uri' => 'https://api.mercadolibre.com']);

        $response = $client->get("/sites/MLA/search?offset={$request->input('offset')}&category=MLA5224&official_store_id=all&access_token=APP_USR-3737365735154941-060817-4b5973ec3fcf1f851ad520ce374e528e-31503793");

        $products = json_decode($response->getBody()->getContents());

        foreach ($products->results as $product) {
            Product::updateOrCreate(['provider_id' => $product->id], [
                'name'      => $product->title,
                'price'     => $product->price,
                'permalink' => $product->permalink,
                'thumbnail' => $product->thumbnail,
                'seller'    => $product->seller->id,
            ]);
        }

        return redirect()->to("/products?offset={$request->input('offset')}");
    }
}