<h1>Products</h1>

<form action="{{ url('/products/save') }}" method="POST" >
    {!! csrf_field() !!}
    {!! method_field('POST') !!}
    <input type="hidden" name="offset" value="{{ $offset }}">
    <button type="submit">Save products</button>
</form>
<ul>
    @foreach($products->results as $product)
        <li>
            <p><strong>Name: </strong>{{ $product->title }}</p>
            <p><strong>Price: </strong>${{ $product->price }}</p>
            <p><a target="_blank" href="{{ $product->permalink  }}"><img src="{{ $product->thumbnail }}" alt=""></a></p>
        </li>
    @endforeach
</ul>